package sample.Controler;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.Chart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import sample.crawler.Student;
import sample.crawler.StudentsParser;

import java.io.File;
import java.io.IOException;

/**
 * Created by Bartek on 2017-04-03.
 */
public class ChartControler {
    XYChart.Series set1;
    public ObservableList<Student> logStudents;
   @FXML
    private BarChart<?,?> MarksChart;
@FXML
private Button RefreshButton;
    @FXML
    private CategoryAxis x;

    @FXML
    private CategoryAxis y;
    public int two,three,threePlus,four,fourPlus,five;

    @FXML
    private void refreshHandleButtonAction(ActionEvent event)  throws IOException {

        if (event.getSource() == RefreshButton) {
            logStudents = FXCollections.observableArrayList(StudentsParser.parse(new File("students.txt")));
            for (Student var : logStudents) {
                if(var.getMark()==2.0)
                {
                    ++two;
                }
                else if(var.getMark()==3.0)
                {
                    ++three;
                }
                else if(var.getMark()==3.5)
                {
                    ++threePlus;
                }
                else if(var.getMark()==4.0)
                {
                    ++four;
                }
                else if(var.getMark()==4.5)
                {
                    ++fourPlus;
                }
                else if(var.getMark()==5.0)
                {
                    ++five;
                }
            }
MarksChart.getData().clear();
            set1 = new XYChart.Series<>();
            set1.getData().add(new XYChart.Data("2.0", two));
            set1.getData().add(new XYChart.Data("3.0", three));
            set1.getData().add(new XYChart.Data("3.5", threePlus));
            set1.getData().add(new XYChart.Data("4.0",four));
            set1.getData().add(new XYChart.Data("4.5", fourPlus));
            set1.getData().add(new XYChart.Data("5", five));
            MarksChart.getData().addAll(set1);
        }
    }
    @FXML
    public void initialize () throws IOException {
        logStudents = FXCollections.observableArrayList(StudentsParser.parse(new File("students.txt")));
        for (Student var : logStudents) {
            if(var.getMark()==2.0)
            {
                ++two;
            }
            else if(var.getMark()==3.0)
            {
                ++three;
            }
            else if(var.getMark()==3.5)
            {
                ++threePlus;
            }
            else if(var.getMark()==4.0)
            {
                ++four;
            }
            else if(var.getMark()==4.5)
            {
                ++fourPlus;
            }
            else if(var.getMark()==5.0)
            {
                ++five;
            }
        }

        set1 = new XYChart.Series<>();
        set1.getData().add(new XYChart.Data("2.0", two));
        set1.getData().add(new XYChart.Data("3.0", three));
        set1.getData().add(new XYChart.Data("3.5", threePlus));
        set1.getData().add(new XYChart.Data("4.0",four));
        set1.getData().add(new XYChart.Data("4.5", fourPlus));
        set1.getData().add(new XYChart.Data("5", five));
        MarksChart.getData().addAll(set1);
    }
}
