package sample.Controler;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import sample.Main;
import sample.crawler.CrawlerTread;
import sample.crawler.GUILogger;
import sample.crawler.Student;
import sample.crawler.StudentsParser;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;

/**
 * Created by Bartek on 2017-04-01.
 */
public class StudentsTableControler   {

   public ObservableList<Student> students;

   public  StudentsTableControler()
 {
students= FXCollections.observableArrayList();
  }


    @FXML
    private TableView<Student> StudentTableView;
    @FXML
    private TableColumn<Student, Double> MarkTabel;
    @FXML
    private TableColumn<Student, String> FirstNameTable;
    @FXML
    private TableColumn<Student, String> LastNameTable;
    @FXML
    private TableColumn<Student, Integer> AgeTable;
    @FXML
    private Button refreshButton;

    @FXML
    private void refreshButtonAction(ActionEvent event) throws Exception{
        if(event.getSource()==refreshButton){
            try {
                students= FXCollections.observableArrayList(StudentsParser.parse(new File( "students.txt" )));
            } catch (IOException e) {
                e.printStackTrace();
            }
            StudentTableView.setItems(students);


        }
    }

    public void AddStudents(Student student)
    {

        students.add(new Student());


    }

    public void  deleteStudent(Student student)
    {
        students.remove(student);
      //  table.setItems(students);
    }

    //public void modifyStudend() throws IOException {
     //   Set<Student> tmp= new HashSet<Student>(StudentsParser.parse(new File( "students.txt" )));

      // for (Student e:tmp) {
      //      students.add(e);

     //   }
        //table.setItems(students);
   // }



   @FXML
   public void initialize()
   {
       MarkTabel.setCellValueFactory(new PropertyValueFactory<>("mark"));
       FirstNameTable.setCellValueFactory(new PropertyValueFactory<>("firstName"));
       LastNameTable.setCellValueFactory(new PropertyValueFactory<>("lastName"));
       AgeTable.setCellValueFactory(new PropertyValueFactory<>("age"));
      try {
         students= FXCollections.observableArrayList(StudentsParser.parse(new File( "students.txt" )));
     } catch (IOException e) {
          e.printStackTrace();
    }
      StudentTableView.setItems(students);



  }



}
