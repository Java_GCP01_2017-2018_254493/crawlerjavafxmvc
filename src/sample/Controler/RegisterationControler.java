package sample.Controler;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.crawler.RegisterClass;
import sample.crawler.Student;

/**
 * Created by Bartek on 2017-03-30.
 */
public class RegisterationControler {

    public ObservableList<RegisterClass> registerPersons=  FXCollections.observableArrayList();
@FXML
private Button CleanButton;
@FXML
private Button SaveButton;
@FXML
private Button CancelButton;


@FXML
private TextField NameTxtField;
@FXML
private TextField AgeTextField;
@FXML
private TextField SexTextField;
@FXML
private PasswordField PasswordTextField;



public ObservableList<RegisterClass> getListOfRegisterPersons()
{
    return registerPersons;
}

    @FXML
    private void cancleHandleButtonAction(ActionEvent event) throws Exception{
        Stage stage;
        Parent root;
        if(event.getSource()==CancelButton){
            //get reference to the button's stage
            stage=(Stage) CancelButton.getScene().getWindow();
            //load up OTHER FXML document
            root = FXMLLoader.load(getClass().getResource("../View/LoginPanel.fxml"));
            //create a new scene with root and set the stage
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        }
    }


    public void cleanHandleButtonAction(ActionEvent event) throws Exception{

        if(event.getSource()==CleanButton){
           NameTxtField.clear();

             AgeTextField.clear();

            SexTextField.clear();

            PasswordTextField.clear();
        }
    }



    @FXML
   public void SaveHandleButtonAction(ActionEvent event) throws Exception{

        if(event.getSource()==SaveButton){
registerPersons.add(new RegisterClass(NameTxtField.textProperty(),PasswordTextField.textProperty(),AgeTextField.textProperty(),SexTextField.textProperty()));

        }
    }
}
