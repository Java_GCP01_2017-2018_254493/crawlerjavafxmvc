package sample.Controler;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import sample.crawler.Student;
import sample.crawler.StudentsParser;

import java.io.File;
import java.io.IOException;

/**
 * Created by Bartek on 2017-04-03.
 */
public class ListControler {
    @FXML
    Button RefreshButton;
    @FXML
    ListView<String> LogListView;
    public ObservableList<Student> parseLogStudents;
    public ObservableList<Student> logStudents;

    @FXML
    private void refreshHandleButtonAction(ActionEvent event)  throws IOException {

        if (event.getSource() == RefreshButton) {
            logStudents = FXCollections.observableArrayList(StudentsParser.parse(new File("students.txt")));
            for (Student var : logStudents) {


                if (!parseLogStudents.contains(var)) {
                    LogListView.getItems().add("DELETE" + var.getMark() + " " + var.getFirstName() + " " + var.getLastName() + " " + var.getAge());
                }

                if (parseLogStudents.size() == logStudents.size()) {
                    if (logStudents.contains(var)) {

                    } else {

                        LogListView.getItems().add("MODIFY" + var.getMark() + " " + var.getFirstName() + " " + var.getLastName() + " " + var.getAge());
                    }

                }
            }
        }
    }
        @FXML
        public void initialize () throws IOException {
            parseLogStudents = FXCollections.observableArrayList(StudentsParser.parse(new File("students.txt")));

            for (Student var : parseLogStudents) {
                LogListView.getItems().add("ADD" + var.getMark() + " " + var.getFirstName() + " " + var.getLastName() + " " + var.getAge());

            }
        }
    }
