package sample.Controler;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.Main;
import sample.crawler.RegisterClass;
import sample.crawler.User;

import java.io.IOException;

/**
 * Created by Bartek on 2017-03-30.
 */
public class LoginPanelControler {
    @FXML
    public Button LoginButton;

    @FXML
    public Button RegisterButton;

@FXML
private TextField LoginTextField;
@FXML
private PasswordField PasswordField;
 private   FXMLLoader loader;
public void addLoder (FXMLLoader _loader)
{
    loader=_loader;
}


    User user = new User();

    private final static String MY_PASS = "bart";
    private final static BooleanProperty accessGranted = new SimpleBooleanProperty(false);
    @FXML
    private void loginHandleButtonAction(ActionEvent event) throws Exception{
        Stage stage;
        Parent root;
        LoginTextField.textProperty().bind(user.userNameProperty());
        PasswordField.setPromptText("Password");
        user.passwordProperty().bind(PasswordField.textProperty());
        PasswordField.textProperty().addListener((obs, ov, nv) -> {
            boolean granted = PasswordField.getText().equals(MY_PASS);
            accessGranted.set(granted);
            if (granted) {
                System.out.println("password:"+ user.getPassword());
            }
        });


        if(event.getSource()==LoginButton){
            //get reference to the button's stage

            if (accessGranted.get())  {
                stage = (Stage) LoginButton.getScene().getWindow();
                //load up OTHER FXML document
                //create a new scene with root and set the stage
                FXMLLoader loader = new FXMLLoader(Main.class.getResource("View/CustomTabPane.fxml"));
                loader.load();
                root = (Parent) loader.getRoot();
             //   MainController controller = (MainController) loader.getController();
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
            }
        }
    }
    @FXML
    private void RegisterHandleButtonAction(ActionEvent event) throws Exception{
        Stage stage;
        Parent root;
        if(event.getSource()==RegisterButton){
            //get reference to the button's stage
            stage=(Stage) RegisterButton.getScene().getWindow();
            //load up OTHER FXML document
            root = FXMLLoader.load(getClass().getResource("../View/Regestration.fxml"));
            //create a new scene with root and set the stage
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        }
    }

 public int check() throws IOException {
      FXMLLoader loader = new FXMLLoader(Main.class.getResource("View/Regestration"));
      loader.load();
    RegisterationControler rC =  loader.getController();
     ObservableList<RegisterClass> registerPersons= rC.registerPersons;

     for (RegisterClass check: registerPersons)
     {
         if(LoginTextField.textProperty().equals(check.nameProperty()));
         return 1;
     }

     return 0;
 }

}
