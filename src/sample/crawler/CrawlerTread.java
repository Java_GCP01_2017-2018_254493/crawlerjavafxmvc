package sample.crawler;



import javafx.fxml.FXMLLoader;
import sample.Main;

import java.io.IOException;

/**
 * Created by Bartek on 2017-03-24.
 */
public class CrawlerTread implements Runnable {

   private FXMLLoader loader;

  public CrawlerTread( FXMLLoader _loader)
  {

      loader=_loader;

  }

    public void run() {
        Crawler proba = new Crawler();


        final Logger[] loggers = new Logger[]
                {
                        new ConsolLogger()
                };
        proba.addIterationStartedListener((i)->{System.out.println("Numer " +i);});
        proba.addToAddStudentsListnerList((student)->{
            String status = proba.isAdd(student);
            for( Logger el : loggers ) {
                el.log(status, student);

            }

        });
        proba.addToDeleteStudentsListnerList((student)->{
            String status = proba.isRemove(student);
            for( Logger el : loggers )
                el.log(status, student);
        });
        proba.addToModifyStudentsListnerList((student)->{
            String status = proba.isModify(student);
            for( Logger el : loggers )
                el.log(status, student);
        });
        try {
            proba.Run();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
