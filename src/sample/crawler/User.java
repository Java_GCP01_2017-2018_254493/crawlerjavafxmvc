package sample.crawler;

import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Bartek on 2017-04-03.
 */
public class User {
    private final ReadOnlyStringWrapper userName;
    private StringProperty password;
    public User() {
        userName = new ReadOnlyStringWrapper(this, "userName", "Bart");
        password = new SimpleStringProperty(this, "password", "");
    }

    public final String getUserName() {
        return userName.get();
    }
    public ReadOnlyStringProperty userNameProperty() {
        return userName.getReadOnlyProperty();
    }

    public final String getPassword() {
        return password.get();
    }
    public StringProperty passwordProperty() {
        return password;
    }
}
