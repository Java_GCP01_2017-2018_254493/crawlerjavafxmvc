package sample.crawler;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Bartek on 2017-04-03.
 */
public class RegisterClass {

    public String getName() {
        return name.get();
    }

    public RegisterClass(StringProperty name, StringProperty password, StringProperty age, StringProperty sex) {
        this.name = name;
        this.password = password;
        this.age = age;
        this.sex = sex;
    }

    public StringProperty nameProperty() {
        return name;
    }

    public String getPassword() {
        return password.get();
    }

    public StringProperty passwordProperty() {
        return password;
    }

    public void setPassword(String password) {
        this.password.set(password);
    }



    public String getSex() {
        return sex.get();
    }

    public StringProperty sexProperty() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex.set(sex);
    }

    public void setName(String name) {

        this.name.set(name);
    }

    private StringProperty name = new SimpleStringProperty();
    private StringProperty password = new SimpleStringProperty();

    public String getAge() {
        return age.get();
    }

    public StringProperty ageProperty() {
        return age;
    }

    public void setAge(String age) {
        this.age.set(age);
    }

    private StringProperty age = new SimpleStringProperty();
    private StringProperty sex = new SimpleStringProperty();





}
