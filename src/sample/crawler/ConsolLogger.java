package sample.crawler;

import javafx.fxml.FXMLLoader;
import sample.Controler.StudentsTableControler;
import sample.Main;

import java.io.IOException;

/**
 * Created by Bartek on 2017-03-18.
 */
public class ConsolLogger implements Logger {
    StudentsTableControler sTC;
    @Override
    public void log(String status, Student student) {
      //  sTC = (StudentsTableControler) loader.getController();
        if(status == "ADDED") {
            System.out.println("ADDED: " + student.getMark() + " " + student.getFirstName() + " " + student.getLastName() + " " + student.getAge());


           sTC.AddStudents(student);
        }

        if(status == "REMOVED") {
            System.out.println("REMOVED: " + student.getMark() + " " + student.getFirstName() + " " + student.getLastName() + " " + student.getAge());
        }
        if(status == "MODIFY") {
            System.out.println("MODIFIED: " + student.getMark() + " " + student.getFirstName() + " " + student.getLastName() + " " + student.getAge());
        }

    }
}
