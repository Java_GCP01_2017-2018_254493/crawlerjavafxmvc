package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.Controler.LoginPanelControler;
import sample.Controler.MainController;
import sample.Controler.StudentsTableControler;
import sample.crawler.CrawlerTread;
import sample.crawler.Student;

import java.io.IOException;

public class Main extends Application   {

    @Override
    public void start(Stage primaryStage) throws Exception{
       // FXMLLoader loader1 = new FXMLLoader(Main.class.getResource("View/StudentsTableView.fxml"));
      //  loader1.load();
       // Thread CrawelerThread = new Thread(new CrawlerTread(loader1));
       // CrawelerThread.start();

        FXMLLoader loader = new FXMLLoader(Main.class.getResource("View/LoginPanel.fxml"));
        loader.load();
       // LoginPanelControler lPC= (LoginPanelControler)loader.getController();
     //   lPC.addLoder(loader1);
     Parent root =(Parent)loader.getRoot();
     primaryStage.setTitle("CrawlerMVC");
     primaryStage.setScene(new Scene(root, 800, 600));
     primaryStage.show();


    }


    public static void main(String[] args) {
        launch(args);
    }
}
